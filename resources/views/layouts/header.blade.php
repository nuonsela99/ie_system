<title>{{config('app.name')}} | @yield('title')</title>
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        {{-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
		
		<!-- Fontawesome CSS -->
        {{-- <link rel="stylesheet" href="assets/css/font-awesome.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">

		<!-- Lineawesome CSS -->
        {{-- <link rel="stylesheet" href="assets/css/line-awesome.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/line-awesome.min.css')}}">

		<!-- Chart CSS -->
		{{-- <link rel="stylesheet" href="assets/plugins/morris/morris.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/plugins/morris/morris.css')}}">

		<!-- Main CSS -->
        {{-- <link rel="stylesheet" href="assets/css/style.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
		
		<!-- Datatable CSS -->
		{{-- <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}">

		<!-- Select2 CSS -->
		{{-- <link rel="stylesheet" href="assets/css/select2.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}">
		
		<!-- Datetimepicker CSS -->
		{{-- <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css"> --}}
		<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">



		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->

		<style>
			.pre_loading{
				margin: 0;
				position:fixed;
				top:50%;
				left: 50%;
				margin-right: -50%;
				transform: translate(-50%,-50%);
				z-index: 1;
				align-items: center;
				display: inline-flex;
			}
		</style>